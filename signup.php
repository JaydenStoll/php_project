<?php
  require "header.php";
 ?>

      <main>
        <h1>Signup</h1>
        <?php
          if (isset($_GET['error'])) {
            if ($_GET['error'] == "emptyfields") {
              echo "<p>Fill in all fields!</p>";
            } else if ($_GET['error'] == "invalidmailuid") {
              // finish the rest of the error handeling
            }
          }
         ?>
        <form action="includes/signup.php" method="post">
          <input type="text" name="uid" placeholder="Username">
          <input type="text" name="mail" placeholder="Email">
          <input type="password" name="upass" placeholder="Password">
          <input type="password" name="upassconf" placeholder="Confirm Password">
          <button type="submit" name="signup-submit">Signup!</button>
        </form>
      </main>

 <?php
    require "footer.php";
  ?>
