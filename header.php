<?php
  session_start();
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <header>
      <nav>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="#">Contact</a></li>
          <li><a href="about-me.php">About me</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
        </ul>
        <div>
          <?php
            if (isset($_SESSION['userID'])) {
              echo '<form class="" action="includes/logout.php" method="post">
                <button type="submit" name="logout-submit">Logout</button>
              </form>';
            } else {
              echo '<form action="includes/login.php" method="post">
                <input type="text" name="_uid" placeholder="Username/Email">
                <input type="password" name="_upass" placeholder="password">
                <button type="submit" name="login-submit">Login</button>
              </form>
              <a href="signup.php">signup</a>';
            }
           ?>
        </div>
      </nav>
    </header>
