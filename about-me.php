<?php
  require "header.php";
 ?>

<?php
  // this will display the amount of people in the Database
    // Ex. 2 entered people will display a 2
  require "includes/database-handler.php";

  if (isset($_SESSION['userID'])) {
    $sql = "SELECT uidUsers FROM users";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      header("Location: about-me.php?error=sqlerror");
      exit();
    } else {
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        $resultCheck = mysqli_stmt_num_rows($stmt);

        echo "Amount of users in the database: ".$resultCheck;
      }
  } else {
    echo "You must be signed in to see this page!";
  }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);
 ?>

 <?php
   require "footer.php";
  ?>
