<?php
  if (isset($_POST['login-submit'])) {
    require 'database-handler.php';

    $mailuid = $_POST['_uid'];
    $pass = $_POST['_upass'];

    if (empty($mailuid) || empty($pass)) {
      header("Location: ../index.php?error=emptyfields");
      exit();
    } else {
      // login with either email or username
      $sql = "SELECT * FROM users WHERE uidUsers=? or umail = ?";
      $stmt = mysqli_stmt_init($conn);
      if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../index.php?error=sqlerror");
        exit();
      } else {
        mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
        mysqli_stmt_execute($stmt);
        $results = mysqli_stmt_get_result($stmt);

        if ($row = mysqli_fetch_assoc($results)) {
          $passCheck = password_verify($pass, $row['upass']);
          if (!$passCheck) {
            header("Location: ../index.php?error=passwordincorrect");
            exit();
          } else {
            session_start();
            $_SESSION['userID'] = $row['pid'];
            $_SESSION['userName'] = $row['uidUsers'];

            header("Location: ../index.php?login=success");
            exit();
          }
        } else {
          header("Location: ../index.php?error=nouser");
          exit();
        }
      }
    }
  } else {
    header("Location: ../index.php");
    exit();
  }
 ?>
